#!/bin/bash
# Makes a link from Geo coordinates copied from Google maps
# Auth: Dom Storey (dstorey@tenable.com) edited by John Walley
#
if [ "$1" == "-h" ]
then
	echo "use:"
	echo "   $(basename $0) [ coords ]"
	echo ""
	echo "where coords are \"LAT, LONG[, ZOOM]\"" 
	echo "Lattitude and Longditude are real numbers separated by a comma and a space."
	echo ""
	echo "Select a place in Google Maps and right-click it. You will see the coordinates."
	echo "Select it and copy the LAT & LONG coordinates to your copy buffer. You"
	echo "can paste these as arguments to the utility, either as arguments to the command."
	echo "or at the prompt if run with no arguments. If you add a third coordinate,"
	echo "you can control the initial zoom level. Format is a number (16-20) and a 'z'."
	echo "If you do not provide a third coordinate, '19z' will be added for you."
	echo "examples:"
	echo "   mkgeo 51.50317869656203, -0.2803776879070974"
	echo "   mkgeo 51.50317869656203, -0.2803776879070974, 17z" 
	exit 1
fi

if [ $# -lt 2 ]
then
	read -p"Enter Coordinates: " COORDS
	COORDS=$(echo $COORDS | tr -d ' ') 
	if [[ "$COORDS" != *"z" ]]
	then
		COORDS="$COORDS,19z"
	fi
else
	COORDS=$1$2${3:-,19z}
fi
echo "https://www.google.com/maps/place/@$COORDS"