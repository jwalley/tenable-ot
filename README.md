### Tenable OT
I've recently begun the process of migrating all my training materials to GitLab.

In this repository, you'll find files from various videos and training sessions I've conducted. You can also access my videos at this [YouTube channel](https://youtube.com/@john.walley).

Here, you'll discover everything from PCAPs to Python scripts utilized for PyTenable, along with other integrations.

I'll ensure that this README file remains current as the content evolves.

Thanks to everyone involved,

John
